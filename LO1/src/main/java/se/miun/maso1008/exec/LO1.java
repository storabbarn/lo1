package se.miun.maso1008.exec;

import java.io.IOException;
//import java.net.MalformedURLException;
import java.net.URL;

import se.miun.maso1008.parser.Parser;
import se.miun.maso1008.dao.SocialAppDAO;

public class LO1 {

	public static void main(String[] args) {
		
		Parser parser = new Parser(); 
		try {
			parser.parseFromUrl(new URL("https://bitbucket.org/javaee2016/javaee16/raw/master/material/events.txt"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		SocialAppDAO socapp = new SocialAppDAO();
		socapp.addGraph(parser.getEvents(), parser.getUsers());		
		System.err.println("Done");
	}
}
