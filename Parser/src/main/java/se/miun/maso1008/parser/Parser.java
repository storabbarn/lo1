package se.miun.maso1008.parser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

// Parser does not need to know how to oersist objects
// That is for the DAO
// import javax.persistence.*;

import se.miun.maso1008.model.*;

public class Parser {

	private Pattern user_ptrn = Pattern
			.compile("(?<firstName>([\\S]*\\s))" + "(?<lastName>([\\S\\D]*\\s))" + "(?<email>([^\\[].*@.*))");
	private Pattern event_ptrn = Pattern
			.compile("(?<title>([^,]*)),\\s*" + "(?<city>([^,]*)),\\s*" + "(?<content>([^,]*)),\\s*"
					+ "(?<startTime>(.*))\\s*-\\s*" + "(?<endTime>(.*))\\s*,\\s*\\[" + "(?<organizers>(.*))\\]");

	private Pattern comment_ptrn = Pattern
			.compile("^\\s(?<userEmail>(.+@.+))\\s+" + "\\((?<time>(.*))\\):\\s*" + "\"(?<comment>(.*))\"");


	private List<User> users = new ArrayList<User>();
	private List<Event> events = new ArrayList<Event>();
	private List<Comment> comments = new ArrayList<Comment>();

	private URL src;
	
	public List<Event> getEvents(){
		return this.events;
	}
	
	public List<Comment> getComments(){
		return this.comments;
	}
	
	public List<User> getUsers(){
		return this.users;
	}
	
	public void parseFromUrl(URL url) throws IOException {
		this.src = url;
		parseStream(new BufferedReader(new InputStreamReader(url.openStream(), StandardCharsets.UTF_8)));
	}

	public void parseStream(BufferedReader br) {
		try {
			String line;

			while ((line = br.readLine()) != null) {

				// Start user part

				if (line.equals("Users:")) {

					// As long as we don't find Events add users
					while (!((line = br.readLine())).equals("Events:")) {

						Matcher matcher = user_ptrn.matcher(line);
						if (matcher.matches()) {
							final User user = new User(matcher.group("firstName").trim(),
									matcher.group("lastName").trim(), matcher.group("email"));
							users.add(user);
							// System.out.println(users);
							continue;
						}
					}

					// Now for Events:
					while ((line = br.readLine()) != null) {

						Matcher matcher = event_ptrn.matcher(line);
						if (matcher.matches()) {

							final String[] organizers = matcher.group("organizers").split(",");

							DateTimeFormatter timeFormat = DateTimeFormatter.ofPattern("yy/MM/dd HH:mm");

							// Create Event
							Event event = new Event(matcher.group("title"), matcher.group("city"),
									LocalDateTime.parse(matcher.group("startTime").trim(), timeFormat),
									LocalDateTime.parse(matcher.group("endTime").trim(), timeFormat),
									matcher.group("content"));

							// Add organizers to Event
							for (String org : organizers) {
								for (User user : users) {
									if (user.getEmail().equals(org.trim())) {
										event.addOrganizer(user);
									}
								}
							}

							// Add comments about this event
							while ((line = br.readLine()) != null) {
								// System.out.println(line);
								if (line.equals("")) {
									// System.out.println("</event>");
									break;
								}

								matcher = comment_ptrn.matcher(line);
								if (matcher.matches()) {

									User author = null;

									for (User user : users) {
										if (user.getEmail().equals(matcher.group("userEmail"))) {
											author = user;
										}
									}

									if (author == null) {
										throw new IOException();
									} else {

										Comment c = new Comment(author, event,
												LocalDateTime.parse(matcher.group("time").trim(), timeFormat),
												matcher.group("comment"));
										comments.add(c);
										event.addComment(c);
										author.addCommentReference(c);

									}

								}
							}
							events.add(event);
						}
					}

				} else {
					throw new IOException();
				}

			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		System.err.println("Parsing completed");
		System.err.println("Source: " + src.toString());
		System.err.println("Events: " + events.size());
		System.err.println("Comments: " + comments.size());
		System.err.println("Users: " + users.size());
		
		for(User user: users){
			System.err.println(user.getId().toString());
		}
		
		System.err.println();
		System.err.println("End report");
		
	
	}
}
