package se.miun.dsv.jee.jpa;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityManager;

import se.miun.maso1008.dao.SocialAppDAO;
import se.miun.maso1008.model.Event;
import se.miun.maso1008.parser.Parser;

public class ImportVerifier implements IImportVerifier {

	EntityManagerFactory emf;
	URL url;

	//
	public ImportVerifier() {
		Parser parser = new Parser();
		try {
			this.url = new URL("https://bitbucket.org/javaee2016/javaee16/raw/master/material/events.txt");

			parser.parseFromUrl(url);
			SocialAppDAO socapp = new SocialAppDAO();

			socapp.addGraph(parser.getEvents(), parser.getUsers());

			this.emf = SocialAppDAO.getEMF();

		} catch (MalformedURLException e) {
			System.err.println("Malformed URL: " + e.toString());
		} catch (UnknownHostException e) {
			System.err.println("Unknown host: " + e.toString());
		} catch (FileNotFoundException e) {
			System.err.println("File not found: " + e.toString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	public List<Event> findAllEventsThatOverlapWithOthers() {

		EntityManager em = emf.createEntityManager();
		return em
				.createQuery(
						"SELECT DISTINCT evt_1 FROM Event evt_1, Event evt_2 WHERE evt_1.startTime < evt_2.endTime AND evt_1.endTime > evt_2.startTime AND evt_1 <> evt_2")
				.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<String> findFullNamesOfUsersHostingFutureEvents() {

		EntityManager em = emf.createEntityManager();

		List<String> strl = (List<String>) em
				.createQuery(
						"SELECT DISTINCT CONCAT(u.firstName, ' ', u.lastName) FROM Event evt, IN (evt.organizers) u WHERE evt.startTime > :now")
				.setParameter("now", Duration.ofDays(14).subtractFrom(LocalDateTime.now())).getResultList();

		for (String s : strl) {
			System.out.println(">>>>> " + s);
		}

		return strl;
	}

	public Long findNumberOfUsersWithMoreThanOneComment() {

		EntityManager em = emf.createEntityManager();

		long ret = (long) em.createQuery("SELECT COUNT(user) FROM User user WHERE size(user.comments) > 1 ")
				.getSingleResult();

		return ret;
	}

	@SuppressWarnings("unchecked")
	public List<Event> findPastEventsInHamburg() {

		EntityManager em = emf.createEntityManager();

		return em
				.createQuery(
						"SELECT DISTINCT evt_1 FROM Event evt_1 WHERE evt_1.city = 'Hamburg' AND evt_1.startTime < :now ")
				.setParameter("now", Duration.ofDays(14).subtractFrom(LocalDateTime.now())).getResultList();
	}
}
