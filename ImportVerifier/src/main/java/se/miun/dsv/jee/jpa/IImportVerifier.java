package se.miun.dsv.jee.jpa;

import java.util.List;

import se.miun.maso1008.model.*;

public interface IImportVerifier {

	public abstract List<Event> findAllEventsThatOverlapWithOthers();

	public abstract List<String> findFullNamesOfUsersHostingFutureEvents();

	public abstract Long findNumberOfUsersWithMoreThanOneComment();

	public abstract List<Event> findPastEventsInHamburg();

}