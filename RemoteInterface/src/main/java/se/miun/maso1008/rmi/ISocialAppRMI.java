package se.miun.maso1008.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;
import java.util.UUID;

import se.miun.maso1008.dto.UserDTO;
import se.miun.maso1008.dto.EventDTO;

public interface ISocialAppRMI extends Remote {
	
	//Populate database
	public void addGraph(List<EventDTO> events, List<UserDTO> users) throws RemoteException;
	
	//User related
	public UserDTO getUserById(UUID id) throws RemoteException;

	public UserDTO getUserByEmail(String email) throws RemoteException;

	public void updateUser(UserDTO user) throws RemoteException;

	public void addUser(UserDTO user) throws RemoteException;

	public List<UserDTO> findAllUsers() throws RemoteException;
	
	//Event related
	public void addEvent(EventDTO event) throws RemoteException;

	public List<EventDTO> findAllEvents() throws RemoteException;

	public List<EventDTO> findEventsInCity(String name) throws RemoteException;
	
	public void updateEvent(EventDTO event) throws RemoteException;
	
	

}
