package se.miun.maso1008.dto;

import java.io.Serializable;
import java.time.LocalDateTime;

public class CommentDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public LocalDateTime getTime() {
		return time;
	}

	public void setTime(LocalDateTime time) {
		this.time = time;
	}

	public String toString() {
		return this.email + " " + this.time + " " + this.comment;
	}

	private String email;
	private String comment;
	private LocalDateTime time;

	public CommentDTO(String email, LocalDateTime time, String comment) {
		this.email = email;
		this.time = time;
		this.comment = comment;
	}
}
