package se.miun.maso1008.dto;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.UUID;

public class EventDTO implements Serializable {

	private String title;
	private String city;
	private String content;
	private LocalDateTime startTime;
	private LocalDateTime endTime;
	private ArrayList<String> organizerEmails;
	private ArrayList<CommentDTO> comments;
	final private UUID id;

	public EventDTO(String title, String city, LocalDateTime startTime, LocalDateTime endTime, String content,
			ArrayList<String> organizerEmails, ArrayList<CommentDTO> comments) {
		this.title = title;
		this.city = city;
		this.startTime = startTime;
		this.endTime = endTime;
		this.content = content;
		this.organizerEmails = organizerEmails;
		this.comments = comments;
		id = UUID.randomUUID();
	}

	private static final long serialVersionUID = 1L;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public LocalDateTime getStartTime() {
		return startTime;
	}

	public void setStartTime(LocalDateTime startTime) {
		this.startTime = startTime;
	}

	public LocalDateTime getEndTime() {
		return endTime;
	}

	public void setEndTime(LocalDateTime endTime) {
		this.endTime = endTime;
	}

	public UUID getId() {
		return id;
	}

	public String toString() {

		String ret = this.id + ", " + this.title + ", " + this.city + ", " + this.content + ", "
				+ this.startTime.toString() + ", " + this.endTime.toString();

		ret += "\n\t";
		try {
			for (String u : this.organizerEmails) {
				ret += u + "\n\t";
			}
			
			
			for (CommentDTO c : this.comments) {
				ret += c.toString() + "\n\t";
			}
		} catch (NullPointerException e) {
			ret += "No organizers\n";
		}

		return ret;
	}
}
