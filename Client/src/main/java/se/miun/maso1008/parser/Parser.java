package se.miun.maso1008.parser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

// Parser does not need to know how to oersist objects
// That is for the DAO
// import javax.persistence.*;

import se.miun.maso1008.dto.*;

public class Parser {

	private Pattern user_ptrn = Pattern
			.compile("(?<firstName>([\\S]*\\s))" + "(?<lastName>([\\S\\D]*\\s))" + "(?<email>([^\\[].*@.*))");
	private Pattern event_ptrn = Pattern
			.compile("(?<title>([^,]*)),\\s*" + "(?<city>([^,]*)),\\s*" + "(?<content>([^,]*)),\\s*"
					+ "(?<startTime>(.*))\\s*-\\s*" + "(?<endTime>(.*))\\s*,\\s*\\[" + "(?<organizers>(.*))\\]");

	private Pattern comment_ptrn = Pattern
			.compile("^\\s(?<userEmail>(.+@.+))\\s+" + "\\((?<time>(.*))\\):\\s*" + "\"(?<comment>(.*))\"");

	private List<UserDTO> users = new ArrayList<UserDTO>();
	private List<EventDTO> events = new ArrayList<EventDTO>();

	private URL src;

	public List<EventDTO> getEvents() {
		return this.events;
	}

	public List<UserDTO> getUsers() {
		return this.users;
	}

	public void parseFromUrl(URL url) throws IOException {
		this.src = url;
		parseStream(new BufferedReader(new InputStreamReader(src.openStream(), StandardCharsets.UTF_8)));
	}

	public void parseStream(BufferedReader br) {
		try {
			String line;

			while ((line = br.readLine()) != null) {

				// Start user part
				if (line.equals("Users:")) {

					// As long as we don't find Events add users
					while (!((line = br.readLine())).equals("Events:")) {

						Matcher matcher = user_ptrn.matcher(line);
						if (matcher.matches()) {
							final UserDTO user = new UserDTO(matcher.group("firstName").trim(),
									matcher.group("lastName").trim(), matcher.group("email"));
							users.add(user);							
							continue;
						}
					}

					DateTimeFormatter timeFormat = DateTimeFormatter.ofPattern("yy/MM/dd HH:mm");

					// Now for Events:
					while ((line = br.readLine()) != null) {

						Matcher matcher = event_ptrn.matcher(line);
						if (matcher.matches()) {
							// Found beginning of an Event
							String title = matcher.group("title");
							String city = matcher.group("city");
							LocalDateTime startTime = LocalDateTime.parse(matcher.group("startTime").trim(),
									timeFormat);
							LocalDateTime endTime = LocalDateTime.parse(matcher.group("endTime").trim(), timeFormat);
							String content = matcher.group("content");

							final String[] organizers = matcher.group("organizers").split(",");

							// Parse organizers into a list of email strings
							ArrayList<String> organizerEmails = new ArrayList<String>();
							
							for (String org : organizers) {
								organizerEmails.add(org.trim());
							}
							ArrayList<CommentDTO> comments = new ArrayList<CommentDTO>();

							// Add comments about this event
							while ((line = br.readLine()) != null) {

								if (line.equals("")) {
									break;// Break out of comment parsing loop
								}
								//
								matcher = comment_ptrn.matcher(line);
								if (matcher.matches()) {

									String authorEmail = matcher.group("userEmail");
									LocalDateTime time = LocalDateTime.parse(matcher.group("time").trim(), timeFormat);
									String comment = matcher.group("comment");

									comments.add(new CommentDTO(authorEmail, time, comment));

								}

							}

							// Create EventDTO
							EventDTO event = new EventDTO(title, city, startTime, endTime, content, organizerEmails,
									comments);

							events.add(event);
						}
					}
				} else {
					throw new IOException();
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
