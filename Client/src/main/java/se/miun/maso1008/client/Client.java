package se.miun.maso1008.client;

import java.net.URL;
import java.rmi.ConnectException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import se.miun.maso1008.parser.Parser;
import se.miun.maso1008.rmi.ISocialAppRMI;

public class Client {

	private String url = "https://bitbucket.org/javaee2016/javaee16/raw/master/material/events.txt";

	public Client(String[] args) {
		try {
			String lookfor = "socialapp";

			Registry reg = LocateRegistry.getRegistry();

			ISocialAppRMI face = (ISocialAppRMI) reg.lookup(lookfor);

			// HashSet<User> new_users = new HashSet<User>();

			// new_users.add(new User("Mårten", "Soderquist",
			// "storabbarn@gmail.com"));
			// face.addUsers(new_users);
			Parser parser = new Parser();

			parser.parseFromUrl(new URL(this.url));
			face.addGraph(parser.getEvents(), parser.getUsers());
			System.out.println("Client connected");
			System.out.println(face.getUserByEmail("ABC").toString());

		} catch (ConnectException e) {
			System.err.println("Connection error. Is server running?");
		} catch (Exception e) {
			System.err.println("Client error " + e.toString());
			e.printStackTrace();

		}
	}

	public static void main(String[] args) {
		Client client = new Client(args);
		
	}

}
