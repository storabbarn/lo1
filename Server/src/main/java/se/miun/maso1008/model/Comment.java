package se.miun.maso1008.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
//import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="comments")
public class Comment implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	//@GeneratedValue
	//TODO Is JPA generated value overwrtiting the given int the ctor?
	@Column(name="comment_id")
	private UUID id;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="user_id")
	private User creator;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="event_id")
	private Event event;
	
	@Column(name="comment")
	@NotNull
	private String comment;
	
	//@Temporal(TemporalType.TIMESTAMP)
	@Column(name="time")
	private LocalDateTime time;
	
	public String toString(){
		return creator.toString(); 
	}
	
	public Comment() {
	}
	
	public Comment( User creator, Event event, LocalDateTime time, String comment ){
		this.creator = creator;
		this.event = event;
		this.time = time;
		this.comment = comment;
		this.id = UUID.randomUUID();
	}
	
	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}
	
	public User getCreator() {
		return creator;
	}

	public void setCreator(User creator) {
		this.creator = creator;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public LocalDateTime getTime() {
		return time;
	}

	public void setTime(LocalDateTime time) {
		this.time = time;
	}

}
