package se.miun.maso1008.server;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;
import java.util.UUID;

import se.miun.maso1008.dao.SocialAppDAO;
import se.miun.maso1008.dto.EventDTO;

//import javax.persistence.EntityManagerFactory;
//import javax.persistence.Persistence;

import se.miun.maso1008.dto.UserDTO;
import se.miun.maso1008.rmi.ISocialAppRMI;

public class Server implements ISocialAppRMI {

	
	private SocialAppDAO dao;
	
	public Server(){
		//TODO Server shall have an instance of of the DAO internally. Create it here
		this.dao = new SocialAppDAO();
	}
	
	
	public static void main(String[] args) {

		try {
			// Create the Server instance. (Here is just static main method)
			// Object to exported.
			
			//Server server = new Server(Persistence.createEntityManagerFactory("socialapp"));
			Server server = new Server();
			
			
			// Export object and store the stub/reference to is as the interface
			// it implements.
			ISocialAppRMI socialAppInterface = (ISocialAppRMI) UnicastRemoteObject.exportObject(server, 0);

			// Now register the stub into the RMI registry. Remember
			// `rmiregistry` needs to be running!
			//Registry reg = LocateRegistry.getRegistry();
			Registry reg = null;
			try{
				reg = LocateRegistry.getRegistry();
				reg.list();
			} catch(RemoteException e) {
				//Communication with the registry failed, attempt to create own
				reg = LocateRegistry.createRegistry(1099);
				System.err.println("Created new registry");
			}
			LocateRegistry.getRegistry(1099);
			// Bind the interface/object to a String reference name. The client
			// can later do a lookup
			// on this to get a reference to the remote object!
			reg.rebind("socialapp", socialAppInterface);
			// Now everything is done
			System.err.println("Server is up!");

		} catch (Exception e) {
			System.err.println("Server error -- " + e.toString());
		}
	}

	@Override
	public UserDTO getUserById(UUID id) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public void addGraph(List<EventDTO> events, List<UserDTO> users) throws RemoteException {
		// TODO Auto-generated method stub
		
	}


	@Override
	public UserDTO getUserByEmail(String email) throws RemoteException {
		return new UserDTO("Banan", "Olle", "test@mail.com");
	}


	@Override
	public void updateUser(UserDTO user) throws RemoteException {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void addUser(UserDTO user) throws RemoteException {
		// TODO Auto-generated method stub
		
	}


	@Override
	public List<UserDTO> findAllUsers() throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public void addEvent(EventDTO event) throws RemoteException {
		// TODO Auto-generated method stub
		
	}


	@Override
	public List<EventDTO> findAllEvents() throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public List<EventDTO> findEventsInCity(String name) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public void updateEvent(EventDTO event) throws RemoteException {
		// TODO Auto-generated method stub
		
	}

//	@Override
//	public void addUsers(Set<User> users) throws RemoteException {
//		
//		EntityManager em = emf.createEntityManager();
//		EntityTransaction tx = em.getTransaction();
//		try {
//			tx.begin();
//
//			for (User u : users) {
//				em.persist(u);
//			}
//
//			tx.commit();
//		} catch (Exception e) {
//			System.err.println("Error commiting to database -- " + e.toString());
//			tx.rollback();
//		} finally {
//			em.close();
//		}
//
//	}
//
//	@Override
//	public void addEvents(Set<Event> events) throws RemoteException {
//		EntityManager em = emf.createEntityManager();
//		EntityTransaction tx = em.getTransaction();
//		try {
//			tx.begin();
//
//			for (Event evt : events) {
//				em.persist(evt);
//			}
//
//			tx.commit();
//		} catch (Exception e) {
//			System.err.println("Error commiting to database -- " + e.toString());
//			tx.rollback();
//		} finally {
//			em.close();
//		}
//
//	}
//
//	@Override
//	public String test() throws RemoteException {
//		return "Hello from Server side";
//	}

}
