package se.miun.maso1008.dao;

import java.util.List;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import se.miun.maso1008.interfaces.ISocialApp;
import se.miun.maso1008.model.Comment;
import se.miun.maso1008.model.Event;
import se.miun.maso1008.model.User;

public class SocialAppDAO implements ISocialApp {

	public static EntityManagerFactory emf = null;
	
	public static  EntityManagerFactory getEMF(){
		if (SocialAppDAO.emf == null) {
			SocialAppDAO.emf = Persistence.createEntityManagerFactory("socialapp");
		}
		return SocialAppDAO.emf;
	}
	
	public SocialAppDAO() {
		if (SocialAppDAO.emf == null) {
			SocialAppDAO.emf = Persistence.createEntityManagerFactory("socialapp");
		}
	}

	@Override
	public void addGraph(List<Event> events, List<User> users) {
		EntityManager manager = emf.createEntityManager();
		EntityTransaction transaction = manager.getTransaction();
		//
		try {
			transaction.begin();

			for (Event event : events) {
				manager.persist(event);
			}

			for (User user : users) {
				manager.persist(user);
			}

			transaction.commit();
		} catch (Exception e) {
			System.err.println("Transaction failed: " + e.toString());
			transaction.rollback();
		} finally {
			manager.close();
		}
		System.err.println("Added graph ! ");
	}

	@Override
	public void addUser(User user) {
		EntityManager manager = emf.createEntityManager();
		EntityTransaction transaction = manager.getTransaction();
		//
		try {
			transaction.begin();

			manager.persist(user);

			transaction.commit();
		} catch (Exception e) {
			System.err.println("Transaction failed: " + e.toString());
			transaction.rollback();
		} finally {
			manager.close();
		}
	}

	@Override
	public User getUserById(UUID id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User getUserByEmail(String email) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addEvent(Event event) {
		// TODO Auto-generated method stub
		EntityManager manager = emf.createEntityManager();
		EntityTransaction transaction = manager.getTransaction();
		//
		try {
			transaction.begin();

			manager.persist(event);

			transaction.commit();
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println("Transaction failed: " + e.toString());
			transaction.rollback();
		} finally {
			manager.close();
		}
	}

	@Override
	public Event getEventById(UUID id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Event> getEventsByCity(String city) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Event> getEventsByOrganizer(User organizer) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addCommentToEvent(Event event, Comment comment) {
		// TODO Auto-generated method stub
		event.addComment(comment);
		this.addEvent(event);
	}
}
