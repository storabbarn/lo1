package se.miun.maso1008.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import javax.persistence.*;
import javax.validation.constraints.*;

@javax.persistence.Entity
@javax.persistence.Table(name = "events")
public class Event implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	//@GeneratedValue
	//TODO Is JPA generated value overwrtiting the given int the ctor?
	@Column(name = "event_id")
	private UUID id;

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public OffsetDateTime getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(OffsetDateTime lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public Set<Comment> getComments() {
		return comments;
	}

	public void setComments(Set<Comment> comments) {
		this.comments = comments;
	}

	@Column(name = "title", length = 50)
	@NotNull
	private String title;

	@Column(name = "city", length = 50)
	@NotNull
	private String city;

	@Column(name = "start_time")
	//@javax.persistence.Temporal(TemporalType.TIMESTAMP)
	@NotNull
	private LocalDateTime startTime;

	@Column(name = "end_time")
	//@javax.persistence.Temporal(TemporalType.TIMESTAMP)
	@NotNull
	private LocalDateTime endTime;

	@Column(name = "content")
	@NotNull
	private String content;

	@Column(name = "last_update")
	//@javax.persistence.Temporal(TemporalType.TIMESTAMP)
	@NotNull
	private OffsetDateTime lastUpdate;

	@ManyToMany
	@JoinTable(name = "event_organizers", joinColumns = @JoinColumn(name = "event_id"), inverseJoinColumns = @JoinColumn(name = "user_id"))
	private Set<User> organizers;

	@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
	@JoinColumn(name="event_id")
	private Set<Comment> comments;

	/**
	 * Constructor without parameters. Required. *
	 */
	public Event() {
	}; // Ctor w/o arg according to requirements

	/**
	 * 
	 * @param title
	 *            Title of Event
	 * @param city
	 *            Name of city where the Event takes place
	 * @param startTime
	 *            When the Event starts
	 * @param endTime
	 *            When the Event ends
	 * @param content
	 *            Description of Event
	 */
	public Event(String title, String city, LocalDateTime startTime, LocalDateTime endTime, String content) {
		this.title = title;
		this.city = city;
		this.startTime = startTime;
		this.endTime = endTime;
		this.content = content;
		this.organizers = new HashSet<User>();
		this.comments = new HashSet<Comment>();
		this.id = UUID.randomUUID();
	}

	/**
	 * Retrieves the start time for the Event as a local time for the Event
	 * location.
	 * 
	 * @return Start time of Event
	 */
	public LocalDateTime getStartTime() {
		return startTime;
	}

	/**
	 * Sets the starting time of the Event
	 * 
	 * @param startTime
	 *            The new start time for the Event
	 */

	public void setStartTime(LocalDateTime startTime) {
		this.startTime = startTime;
	}

	/**
	 * Retrieves the end time for the Event in local time for the location
	 * 
	 * @return
	 */
	public LocalDateTime getEndTime() {
		return endTime;
	}

	/**
	 * Sets the end time of the Event. Time is expected to be in Event location
	 * local time.
	 * 
	 * @param endTime
	 *            The new end time of the Event
	 */
	public void setEndTime(LocalDateTime endTime) {
		this.endTime = endTime;
	}

	/**
	 * Sets the last updated time to the current time.
	 * 
	 * @return The new last updated time
	 */
	
	
	/**
	 * Called when object is first persisted into database 
	 * 
	 * Perhaps redundant as it just calls this.touch() which is @PreUpdate
	 */
	@PrePersist
	public void onCreate() {
		this.touch();
	}
	
	/**
	 * Called when object is updated in database
	 */
	@PreUpdate
	public void touch() {
		this.lastUpdate = OffsetDateTime.now();
	}

	/**
	 * Gets all orgnazing Users
	 * 
	 * @return A Set of all Users that are registered as Organizers for the
	 *         Event
	 */
	public Set<User> getOrganizers() {
		return organizers;
	}

	/**
	 * Replaces the list of Users currently listed as Organizers with the
	 * supplied list.
	 * 
	 * @param organizers
	 *            A Set of Users to replace the current list with.
	 */
	public void setOrganizers(Set<User> organizers) {
		this.organizers = organizers;
	}

	/**
	 * Add a User to the list of organizers
	 * 
	 * @param organizer
	 *            The User to add
	 */
	public void addOrganizer(User organizer) {
		this.organizers.add(organizer);
		//this.touch(); //Use @PreUpdate instead
	}

	/**
	 * Adds a comment about this event. Note that adding a comment does not
	 * update the lastUpdate field for the Event.
	 * 
	 * @param comment
	 *            A comment.
	 */
	public void addComment(Comment comment) {
		this.comments.add(comment);
	}

	/**
	 * Retrieve Title of Event.
	 * 
	 * @return The title of the Event.
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Set Title of Event
	 * 
	 * @param title
	 *            The new title of the Event
	 */
	public void setTitle(String title) {
		this.title = title;
		this.touch();
	}

	/**
	 * Get the name of the city where the Event tales place.
	 * 
	 * @return The city name where the Event takes place
	 */
	public String getCity() {
		return city;
	}

	/**
	 * Set the name of the city wher ethe event takes place.
	 * 
	 * @param city
	 */
	public void setCity(String city) {
		this.city = city;
		this.touch();
	}

	/**
	 * Retrieve the content of the Event
	 * 
	 * @return The content description of the Event
	 */
	public String getContent() {
		return content;
	}

	/**
	 * Set the content description of the Event
	 * 
	 * @param content
	 *            The new content desciption of the Event
	 */
	public void setContent(String content) {
		this.content = content;
		this.touch();
	}
	
	@Override
	public String toString(){
		return this.city + ", " + this.title;
	}
}
