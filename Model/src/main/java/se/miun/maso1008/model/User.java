package se.miun.maso1008.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="users")
public class User implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	//@GeneratedValue
	//TODO Is JPA generated value overwrtiting the given int the ctor?
	@Column(name = "user_id")
	private UUID id;

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}
	
	@ManyToMany(mappedBy="organizers")//The field in the Event class which contains the Users organizing
	private Set<Event> organizing = new HashSet<Event>();
	
	
	@OneToMany(mappedBy="creator")
	private Set<Comment> comments = new HashSet<Comment>();
	
	public void addCommentReference(Comment comment){
		this.comments.add(comment);
	}
	
	public Set<Comment> getComments() {
		return comments;
	}

	public void setComments(Set<Comment> comments) {
		this.comments = comments;
	}

	public void addEventOrganizing(Event evt){
		this.organizing.add(evt);
	}
	
	public Set<Event> getOrganizing() {
		return organizing;
	}

	public void setOrganizing(Set<Event> organizing) {
		this.organizing = organizing;
	}

	@Column(name = "first_name", length = 32)
	@NotNull
	private String firstName;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Column(name = "last_name", length = 32)
	@NotNull
	private String lastName;

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Column(name = "email", length = 255, unique=true)
	@NotNull
	private String email;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public User() {
	}

	public User(String firstName, String lastName, String email) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.id = UUID.randomUUID();
	}

	public String toString() {
		return new String(firstName + " " + lastName);
	}
}
