package se.miun.maso1008.interfaces;

import java.util.List;
import java.util.UUID;

import se.miun.maso1008.model.Event;
import se.miun.maso1008.model.User;
import se.miun.maso1008.model.Comment;

public interface ISocialApp {

	/**
	 * @brief Persists the supplied objects.
	 * 
	 * @param events A List of Event objects to persist
	 * @param users A List of User objects to persist 
	 * 
	 */
	public void addGraph(List<Event> events, List<User> users);
	
	public void addUser(User user);
	public User getUserById(UUID id);
	public User getUserByEmail(String email);
	
	public void addEvent(Event event);
	public Event getEventById(UUID id);
	public List<Event> getEventsByCity(String city);
	public List<Event> getEventsByOrganizer(User organizer);
	
	public void addCommentToEvent(Event event, Comment comment);
	
	
	
}
